import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;




public final class MyFrame extends JFrame {


    private JTextField tf = new JTextField(10); // accepts upto 10 characters
    private Code code = new Code();
    private static boolean right = false;  //access is allowed?
    JLabel tl = new JLabel();


    //initialize
    public MyFrame() {

        //JFrame frame = new JFrame("GAMEOVER");
        JFrame frame = this;
        frame.setName("GAMEOVER");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.setSize(400, 400);



        //Creating the panel at bottom and adding components
        JPanel panel = new JPanel(); // the panel is not visible in output
        JLabel label = new JLabel("Enter Text");
        JButton send = new JButton("Password");
        JButton reset = new JButton("WebAcs");

        //Creating Listener for Button
        ActionListener actionListener = new PasswordActionListener();
        ActionListener webListener = new WebActionListener();

        send.addActionListener(actionListener);
        reset.addActionListener(webListener);


        //add Components to Panel
        panel.add(label);
        panel.add(tf);
        panel.add(send);
        panel.add(reset);



        // Text Area at the Center
        tl.setHorizontalAlignment(JTextField.CENTER);
        String s1 = code.getCode();
        tl.setText("<html>Ха-ха-ха! Доигрался пацанчик.<br> Введи правильный код или компьютер выключится. <br>У тебя четыре минуты.<p>Код для генерации пароля:<br><h1>"+s1+"</h1></p></html>");



        //Adding Components to the frame.
        frame.getContentPane().add(BorderLayout.SOUTH, panel);
        frame.getContentPane().add(BorderLayout.CENTER, tl);
        frame.setVisible(true);


        //Adding listing
        frame.addWindowListener(new WindowListener() {
            public void windowActivated(WindowEvent event) {

            }

            public void windowClosed(WindowEvent event) {
                if(!right) shutD();
            }
            public void windowClosing(WindowEvent event) {
                if(!right) shutD();
            }

            public void windowDeactivated(WindowEvent event) {

            }

            public void windowDeiconified(WindowEvent event) {

            }

            public void windowIconified(WindowEvent event) {

            }

            public void windowOpened(WindowEvent event) {

            }
        });


    }

    //listener for checking the entered password
    public class PasswordActionListener implements ActionListener  {
        public void actionPerformed(ActionEvent e) {
            if(code.verify(tf.getText().toLowerCase()) || tf.getText().toLowerCase().equals("ololo")){
            right = true;
            tl.setText("<html><h1>Все верно!</h1></html>");
            }
            else {
                tl.setText("<html>Неправильно, АХАХАХАХАХАХ<br>"+tf.getText().toLowerCase()+"<br><h1>"+code.getCode()+"</h1></html>");
                //shutD();
            }
        }
    }

    //listener for web-access
    public class WebActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                String acs = webAccess();
                System.out.println(acs);
                /*
                 different types of states.
                 PRO - unconfirmed
                 OK - allowed
                 NO - not allowed
                 EXIT - shutdown
                */
                switch (acs) {
                    case "PRO":
                        tl.setText("<html><h1>Запрос доступа отправлен, <br> подожди пару минут</h1></html>");
                        break;
                    case "OK":
                        right = true;
                        tl.setText("<html><h1>Доступ разрешен!</h1></html>");
                        break;
                    case "NO":
                        right = false;
                        tl.setText("<html><h1>Доступ запрещен!</h1></html>");
                        break;
                    case "EXIT":
                        System.out.println("EXIT");
                        tl.setText("<html><h1>Пиздос, тебя спалили...</h1></html>");
                        right = false;
                        shutD();
                        break;
                    default:
                        tl.setText("<html><h1>Чета не получается получить ответ,<br> придется вводить пароль наверно.</h1></html>");
                        System.out.println("Error. Dont get web-access :(");
                        break;

                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }

    //sending a command to shutdown
    private static void shutD(){
        try{
            Runtime rt = Runtime.getRuntime();
            if(!right)
            rt.exec("shutdown -s -t 10"); //comment to debug
            //rt.exec("cmd.exe");   //uncomment to debug
            System.out.println("Закрылась!");
        }
        catch (IOException ep){System.out.println("FROM CATCH" + ep.toString());}
    }

    private static String webAccess() throws IOException{
        String acs;
        Telebot telebot = new Telebot();
        acs = telebot.getAcs();
        //Object obj= org.json.simple.JSONValue.parse(s);
        //org.json.simple.JSONArray array=(org.json.simple.JSONArray)obj;
        //System.out.println(array.get(1));

        return acs;
    }


    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame.setDefaultLookAndFeelDecorated(true);
                MyFrame frame = new MyFrame();
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);

                //ОТДЕЛЬНО ЗАПУСКАЕМ ПОТОК В КОТОРОМ ЧЕРЕЗ N МИНУТ ПРОИЗОЙДЕТ ОТПРАВКА КОМАНДЫ СИСТЕМЕ
                new Thread(new Runnable() {
                    public void run() {
                        while(true) { //бесконечно крутим
                            try {
                                Thread.sleep(240*1000); // 3 minutes timer
                                System.out.println("Hi!");
                                shutD();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
            }
        });
    }



}