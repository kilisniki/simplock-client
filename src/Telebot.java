import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Telebot {
    private static String address = "http://undegr.ru:3000/index2/try"; //for First request
    private static String address2 = "http://undegr.ru:3000/index2/acs"; //for Second and later requests


    public String getAcs() throws IOException {
        String jsonn="";
        URL url = new URL(address);
        BufferedReader br;
        try {
            br = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
        }
        catch (Exception e){
            return "error";
        }

        String s;
        while((s =br.readLine())!=null)
        {
            System.out.println(s);
            jsonn = s;
        }
        br.close();


        //For Debug
        /*
        JSONObject obj=new JSONObject();
        obj.put("time","2018-06-24T00:36:21.968Z");
        obj.put("version","PROCESS");
        System.out.println("Name:"+obj.get("name"));
        System.out.println("Age:"+obj.get("age"));
        JSONObject ar=(JSONObject)JSONValue.parse(obj.toString());
        JSONArray ar=(JSONArray)JSONValue.parse(s);
        */
        JSONObject art=(JSONObject)JSONValue.parse(jsonn);


        //System.out.println(jsonn);
        //System.out.println(art);

        //switch request address
        address = address2;
        return art.get("access").toString();
    }

    /*public static void main() throws IOException{
        Telebot telebot = new Telebot();
        telebot.getAcs();
    }*/


}
