import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;


public class Code {
    private static final String mCHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";  //ALPHABET
    private static  final String salt = "123";
    private static final int STR_LENGTH = 6; //
    private String code;

    private String md5Code;

    public Code(){

        //String code;
        Random random = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < STR_LENGTH; i++) {
            int number = random.nextInt(mCHAR.length());
            char ch = mCHAR.charAt(number);
            builder.append(ch);
        }
        code = builder.toString();
        //md5Code = md5(code);      //WITHOUT SALT
        md5Code = md5(code+salt);
    }

    private String md5(String s){
        try {
            //s = "TEST STRING";
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(s.getBytes(),0,s.length());
            String signature = new BigInteger(1,md5.digest()).toString(16);
            s = signature;
            //System.out.println("Signature: "+signature);

        } catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


        char cx[] = new char[6];

        //for more "security"
        // Example: s = 86abdfc63689fb0a0c8df5...
        //    then cx = bdfc63
        s.getChars(3,9, cx,0);


        //if you do not want to break the brain
        //s.getChars(0,9,cx,0);


        //for debag
        /*System.out.println("s = "+s+", cx = "+new String(cx));
        System.out.println("codemd5: "+cx[0]+cx[1]+cx[2]+cx[3]+cx[4]+cx[5]);
        s = ""+cx[0]+cx[1]+cx[2]+cx[3]+cx[4]+cx[5];
        System.out.println("md5: "+s);*/

        return new String(cx);
    }

    public Boolean verify(String usercode){
        if(usercode.equals(md5Code))
            return true;
        else
            return false;
    }
    public String getCode(){
        return code;
    }
    /*public String getMd5(){
        return md5Code;
    }*/
}
